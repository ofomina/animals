﻿using Animals;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Runtime.Serialization;

namespace UnitTests
{
    
    
    /// <summary>
    ///This is a test class for OneModelAnimalSerializerTest and is intended
    ///to contain all OneModelAnimalSerializerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class OneModelAnimalSerializerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for OneModelAnimalSerializer`1 Constructor
        ///</summary>
        public void OneModelAnimalSerializerConstructorTestHelper<T>()
        {
            OneModelAnimalSerializer<T> target = new OneModelAnimalSerializer<T>();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        [TestMethod()]
        public void OneModelAnimalSerializerConstructorTest()
        {
            OneModelAnimalSerializerConstructorTestHelper<GenericParameterHelper>();
        }

        /// <summary>
        ///A test for OneModelAnimalSerializer`1 Constructor
        ///</summary>
        public void OneModelAnimalSerializerConstructorTest1Helper<T>()
        {
            DataContractSerializer context = null; // TODO: Initialize to an appropriate value
            string modelName = string.Empty; // TODO: Initialize to an appropriate value
            OneModelAnimalSerializer<T> target = new OneModelAnimalSerializer<T>(context, modelName);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        [TestMethod()]
        public void OneModelAnimalSerializerConstructorTest1()
        {
            OneModelAnimalSerializerConstructorTest1Helper<GenericParameterHelper>();
        }

        /// <summary>
        ///A test for Deserialize
        ///</summary>
        public void DeserializeTestHelper<T>()
        {
            OneModelAnimalSerializer<T> target = new OneModelAnimalSerializer<T>(); // TODO: Initialize to an appropriate value
            object expected = null; // TODO: Initialize to an appropriate value
            object actual;
            actual = target.Deserialize();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        [TestMethod()]
        public void DeserializeTest()
        {
            DeserializeTestHelper<GenericParameterHelper>();
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        public void DisposeTestHelper<T>()
        {
            OneModelAnimalSerializer_Accessor<T> target = new OneModelAnimalSerializer_Accessor<T>(); // TODO: Initialize to an appropriate value
            bool p = false; // TODO: Initialize to an appropriate value
            target.Dispose(p);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        [TestMethod()]
        [DeploymentItem("Animals.exe")]
        public void DisposeTest()
        {
            DisposeTestHelper<GenericParameterHelper>();
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        public void DisposeTest1Helper<T>()
        {
            OneModelAnimalSerializer<T> target = new OneModelAnimalSerializer<T>(); // TODO: Initialize to an appropriate value
            target.Dispose();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        [TestMethod()]
        public void DisposeTest1()
        {
            DisposeTest1Helper<GenericParameterHelper>();
        }

        /// <summary>
        ///A test for Finalize
        ///</summary>
        public void FinalizeTestHelper<T>()
        {
            OneModelAnimalSerializer_Accessor<T> target = new OneModelAnimalSerializer_Accessor<T>(); // TODO: Initialize to an appropriate value
            target.Finalize();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        [TestMethod()]
        [DeploymentItem("Animals.exe")]
        public void FinalizeTest()
        {
            FinalizeTestHelper<GenericParameterHelper>();
        }

        /// <summary>
        ///A test for Serialize
        ///</summary>
        public void SerializeTestHelper<T>()
        {
            OneModelAnimalSerializer<T> target = new OneModelAnimalSerializer<T>(); // TODO: Initialize to an appropriate value
            object value = null; // TODO: Initialize to an appropriate value
            target.Serialize(value);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        [TestMethod()]
        public void SerializeTest()
        {
            SerializeTestHelper<GenericParameterHelper>();
        }

        /// <summary>
        ///A test for checkAlgorithmData
        ///</summary>
        public void checkAlgorithmDataTestHelper<T>()
        {
            OneModelAnimalSerializer_Accessor<T> target = new OneModelAnimalSerializer_Accessor<T>(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.checkAlgorithmData();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        [TestMethod()]
        [DeploymentItem("Animals.exe")]
        public void checkAlgorithmDataTest()
        {
            checkAlgorithmDataTestHelper<GenericParameterHelper>();
        }
    }
}
