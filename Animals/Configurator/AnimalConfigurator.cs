﻿/// -----------------------------------------------------------------------
/// <copyright file="AnimalConfigurator.cs" company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Configurator
{
    
    #region using

    using System;
    using System.Reflection;


    using Animals.Interfaces;
    using Animals.Configuration;


    #endregion

    /// <summary>
    /// Animal type instance configurator
    /// </summary>
    public class AnimalConfigurator : IEntityConfigurator
    {

        /// <summary>
        /// Parameterless constructor
        /// </summary>
        public AnimalConfigurator()
        {
            m_SpeciesItems = ConfigBuffer.SpeciesItems;
        }

        /// <summary>
        /// Constructor with parameter
        /// </summary>
        /// <param name="connectionString"></param>
        public AnimalConfigurator(string connectionString)
        {
            ConfigBuffer.ConnectionString = connectionString;
            m_SpeciesItems = ConfigBuffer.SpeciesItems;
        }

        /// <summary>
        /// Configures instance of animal type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="element"></param>
        /// <exception cref="InvalidCastException: property value conversion error"
        /// <exception cref="FormatException: property value conversion error"
        /// <exception cref="OverflowException: property value conversion error"
        /// <exception cref="TargetInvocationException: property set error"
        public void Configure<T>(T element)
        {
            Type typeToConfig = typeof(T);

            PropertyInfo propertyInfo = default(PropertyInfo);
            Type propertyType = default(Type);
            string propertyValue = default(string);

            if (!typeToConfig.Equals(Type.GetType("Animals.Model.AnimalBase")))
            {
                var t = typeof(AnimalConfigurator).GetMethod("Configure").MakeGenericMethod(typeToConfig.BaseType);
                t.Invoke(this, new object[] { element });
            }

            foreach (var species in m_SpeciesItems)
            {
                if (((SpeciesElement)species).Name.Equals(typeToConfig.FullName))
                {
                    foreach (var property in ((SpeciesElement)species).SpeciesProperties)
                    {
                        propertyInfo = typeToConfig.GetProperty(((SpeciesPropertyElement)property).Name);
                        propertyType = Type.GetType(((SpeciesPropertyElement)property).Type);
                        propertyValue = ((SpeciesPropertyElement)property).Value;

                        try
                        {
                            var value = Convert.ChangeType(propertyValue, propertyType);
                            propertyInfo.SetValue(element, value, null);
                        }
                        catch (InvalidCastException err)
                        {
                            Console.WriteLine("Animal property conversion error", err);
                        }
                        catch (FormatException err)
                        {
                            Console.WriteLine("Animal property conversion error", err);
                        }
                        catch (OverflowException err)
                        {
                            Console.WriteLine("Animal property conversion error", err);
                        }
                        catch (TargetInvocationException err)
                        {
                            Console.WriteLine("Animal property set error", err);
                        }
                    }
                    break;
                }
            }

            
        }


        #region Private fields

        private SpeciesCollection m_SpeciesItems = null;

        #endregion

    }
}
