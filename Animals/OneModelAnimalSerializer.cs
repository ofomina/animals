﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml;
using Animals.Interfaces;
using Animals.Model;
using System.Collections;
using Application.Testing.XmlRepository.Helpers;

namespace Animals
{
    internal class OneModelAnimalSerializer<T> : IDisposable, ISerializerContract where T:IStorageEntity
    {
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="modelName"></param>
        public OneModelAnimalSerializer(DataContractSerializer context, string modelName)
        {
            m_Context = context;
            m_ModelName = modelName;
        }

        public OneModelAnimalSerializer()
        {
 
        }

        #region IDisposable implementation

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool p)
        {
            if (m_Disposed)
                return;
            ///TODO dispose logic here
            ///
            m_Disposed = true;
        }

        ~OneModelAnimalSerializer()
        {
            Dispose(false);
        }

        private bool m_Disposed = false;

        #endregion

        #region ISerializerContract implementations

        public void Serialize(object value)
        {

            if (!checkAlgorithmData())
                throw new InvalidOperationException();

            if (!(value is AnimalBase))
                throw new System.NotSupportedException();

            DataContractSerializationHelper dataContractSerializationHelper = new DataContractSerializationHelper();
            dataContractSerializationHelper.SaveModel(m_ModelName, (T)value);
        }

        public object Deserialize()
        {
            if (!checkAlgorithmData())
                throw new InvalidOperationException();

            var t = typeof(T);
            var ab = typeof(AnimalBase);
            if (!(t.IsSubclassOf(ab)))
                throw new System.NotSupportedException();

            DataContractSerializationHelper dataContractSerializationHelper = new DataContractSerializationHelper();
            return dataContractSerializationHelper.LoadModel<T>(m_ModelName);
        }

        private bool checkAlgorithmData()
        {
            return (m_Context != null & !string.IsNullOrEmpty(m_ModelName));
        }

        #endregion

        private readonly DataContractSerializer m_Context = default(DataContractSerializer);

        private readonly string m_ModelName = default(string);
    }
}
