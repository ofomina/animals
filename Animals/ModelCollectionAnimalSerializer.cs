﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml;
using Animals.Interfaces;
using Animals.Model;
using System.Collections;
using Application.Testing.XmlRepository.Helpers;

namespace Animals
{
    internal class ModelCollectionAnimalSerializer<T> : IDisposable, ISerializerContract where T : IStorageEntity
    {

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="modelName"></param>
        public ModelCollectionAnimalSerializer(DataContractSerializer context, string modelName)
        {
            m_Context = context;
            m_ModelName = modelName;
        }

        public ModelCollectionAnimalSerializer()
        {
            // TODO: Complete member initialization
        }

        #region IDisposable implementation

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool p)
        {
            if (m_Disposed)
                return;
            ///TODO dispose logic here
            ///
            m_Disposed = true;
        }

        ~ModelCollectionAnimalSerializer()
        {
            Dispose(false);
        }

        private bool m_Disposed = false;

        #endregion

        #region ISerializerContract implementations

        public void Serialize(object value)
        {

            if (!checkAlgorithmData())
                throw new InvalidOperationException();

            T valueType = (T)value;
            if (!(valueType is ICollection))
                throw new System.NotSupportedException();
            
            DataContractSerializationHelper dataContractSerializationHelper = new DataContractSerializationHelper();
            dataContractSerializationHelper.SaveModel(m_ModelName, (T)value);
        }

        public object Deserialize()
        {
            if (!checkAlgorithmData())
                throw new InvalidOperationException();
            
            var t = typeof(T);
            if (t.GetInterface("ICollection") == null)
                throw new System.NotSupportedException();

            DataContractSerializationHelper dataContractSerializationHelper = new DataContractSerializationHelper();
            return dataContractSerializationHelper.LoadModel<T> (m_ModelName);
        }

        private bool checkAlgorithmData()
        {
            return (m_Context != null & !string.IsNullOrEmpty(m_ModelName));
        }

        #endregion

        private readonly DataContractSerializer m_Context = default(DataContractSerializer);

        private readonly string m_ModelName = default(string);
    }
}