﻿namespace Animals
{

    #region using

    using Animals.Model;
    using Animals.Interfaces;
    using Animals.Configuration;

    using Autofac;
    

    using System;
    using System.Reflection;
    using Animals.Configurator;


    #endregion


    class Program
    {
        static void Main(string[] args)
        {

            LogicContainer logicContainer = (LogicContainer)LogicContainer.Instance;

            SpecPanolisFlammea specPanolisFlammeaUserConf = logicContainer.GetService<SpecPanolisFlammea>("");
            //SpecPanolisFlammea specPanolisFlammeaUserConf = logicContainer.GetService<SpecPanolisFlammea>("FilePath=D:\\\\app.config; ConfigurationUserLevel=None");
            

            Console.WriteLine("User app config:");
            Console.WriteLine("MoltsNumber: {0}", specPanolisFlammeaUserConf.MoltsNumber);
            Console.WriteLine("MaxWingSpan: {0}", specPanolisFlammeaUserConf.MaxWingSpan);



            Console.ReadLine();

            /// TRASH:
            //NewAnimalBuild();
            //AnimalSerialization();
            //AnimalModelLoad();

        }
        
        /// <summary>
        /// AnimalSerialization implementation
        /// </summary>
        private static void AnimalSerialization()
        {
            #region single instance serialization

            ModelName = @"D:\Models.xml";
            Context = new System.Runtime.Serialization.DataContractSerializer(typeof(SpecPapilioMachaon));

            SpecPapilioMachaon papilioMachaon = new SpecPapilioMachaon();
            //papilioMachaon.Name = "PapilioMachaon12";

            // Serialization using AnimalSerializer
            using (ISerializerContract serializer = new AnimalSerializer<SpecPapilioMachaon>(Context, ModelName))
            {
                serializer.Serialize((object)papilioMachaon);
            }

            object papilioMachaon1 = new SpecPapilioMachaon();

            using (ISerializerContract serializer = new AnimalSerializer<SpecPapilioMachaon>(Context, ModelName))
            {
                papilioMachaon1 = serializer.Deserialize();
            }

            // Serialization using AnimalSerializerAutofac
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterModule(new AnimalSerializerAutofac<SpecPapilioMachaon>(Context, ModelName) { });

            IContainer container = builder.Build();

            ISerializerContract af_serializer = container.Resolve<ISerializerContract>();
            af_serializer.Serialize(papilioMachaon);
            papilioMachaon1 = af_serializer.Deserialize();

            #endregion


            #region collection serialization

            ModelName = @"D:\Models1.xml";
            Context = new System.Runtime.Serialization.DataContractSerializer(typeof(AnimalCollection<SpecPapilioMachaon>));

            AnimalCollection<SpecPapilioMachaon> papilioMachaonCollection = new AnimalCollection<SpecPapilioMachaon>();
            papilioMachaonCollection.Add(new SpecPapilioMachaon());
            papilioMachaonCollection.Add(new SpecPapilioMachaon());
            papilioMachaonCollection.Add(new SpecPapilioMachaon());
            papilioMachaonCollection.Add(new SpecPapilioMachaon());

            // Serialization using AnimalSerializer
            using (ISerializerContract serializer = new AnimalSerializer<AnimalCollection<SpecPapilioMachaon>>(Context, ModelName))
            {
                serializer.Serialize(papilioMachaonCollection);
            }

            AnimalCollection<SpecPapilioMachaon> papilioMachaonCollection1 = new AnimalCollection<SpecPapilioMachaon>();

            using (ISerializerContract serializer = new AnimalSerializer<AnimalCollection<SpecPapilioMachaon>>(Context, ModelName))
            {
                papilioMachaonCollection1 = (AnimalCollection<SpecPapilioMachaon>)serializer.Deserialize();
            }

            // Serialization using AnimalSerializerAutofac
            ContainerBuilder builder1 = new ContainerBuilder();
            builder1.RegisterModule(new AnimalSerializerAutofac<AnimalCollection<SpecPapilioMachaon>>(Context, ModelName) { });

            IContainer container1 = builder1.Build();

            Type type = papilioMachaonCollection.GetType();
            af_serializer = container1.Resolve<ISerializerContract>();
            af_serializer.Serialize(papilioMachaonCollection);
            papilioMachaonCollection1 = (AnimalCollection<SpecPapilioMachaon>)af_serializer.Deserialize();

            #endregion
        }

        public static string ModelName { get; set; }

        public static System.Runtime.Serialization.DataContractSerializer Context { get; set; }

        /// <summary>
        /// Loads AnimalModel from configuration file
        /// </summary>
        public static void AnimalModelLoad()
        {

            if (!ConfigBuffer.IsInitialized)
                ConfigBuffer.Initialize();

            SpeciesCollection speciesItems = ConfigBuffer.SpeciesItems;

            Console.WriteLine("Type:");
            Console.WriteLine("---------------------");
            foreach (var species in speciesItems)
            {
                Console.WriteLine(((SpeciesElement)species).Name);
            }

            Console.WriteLine("");
            Console.WriteLine("Properties:");
            Console.WriteLine("---------------------");
            foreach (var species in speciesItems)
            {
                foreach (var property in ((SpeciesElement)species).SpeciesProperties)
                {
                    Console.WriteLine(((SpeciesPropertyElement)property).Name);
                }
            }

            Console.WriteLine("");
            Console.WriteLine("Methods:");
            Console.WriteLine("---------------------");
            foreach (var species in speciesItems)
            {
                foreach (var method in ((SpeciesElement)species).SpeciesMethods)
                {
                    Console.WriteLine(((SpeciesMethodElement)method).Name);
                }
            }


            Console.ReadLine();
        }

        /// <summary>
        /// Defines NewAnimal dynamic type
        /// </summary>
        public static void NewAnimalBuild()
        {
            LogicContainer container = (LogicContainer)LogicContainer.Instance;
            INewAnimal newAnimal = (INewAnimal)container.GetService(typeof(INewAnimal));

            Type t = newAnimal.GetType();
            MethodInfo[] Methods = t.GetMethods();
            PropertyInfo[] Properties = t.GetProperties();

            Console.WriteLine("Methods:");
            Console.WriteLine("---------");

            foreach (var item in Methods)
            {
                Console.WriteLine(item.Name);
            }

            Console.WriteLine("");
            Console.WriteLine("Properties:");
            Console.WriteLine("---------");

            foreach (var item in Properties)
            {
                Console.WriteLine(item.Name);
            }

            Console.WriteLine("");
            Console.WriteLine("Property Class test:");
            Console.WriteLine("---------");

            PropertyInfo pi = t.GetProperty("Class");

            Console.WriteLine("newAnimal.Class: {0}", pi.GetValue(newAnimal, null));
            pi.SetValue(newAnimal, "ClassValue", null);
            Console.WriteLine("newAnimal.Class: {0}", pi.GetValue(newAnimal, null));

        }
    }
}
