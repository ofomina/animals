﻿// -----------------------------------------------------------------------
// <copyright file="DataContractSerializationHelper.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Application.Testing.XmlRepository.Helpers
{
    using System;
    using System.Xml;
    using System.IO;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Animals.Interfaces;

    public class DataContractSerializationHelper
    {
        public IEnumerable<T> LoadModelsCollection<T>(string fullModelName) where T : IStorageEntity
        {
            var ds = new DataContractSerializer(typeof(IEnumerable<T>));
            var settings = new XmlReaderSettings { };

            using (var r = XmlReader.Create(fullModelName, settings))
            {
                return (IEnumerable<T>)ds.ReadObject(r);
            }
        }


        public IEnumerable<T> LoadModelsCollection<T>(string folder, string customModelName = null) where T : IStorageEntity
        {
            string fullModelName = Path.Combine(folder, string.Format("{0}Collection.xml", typeof(T).Name));

            if (!string.IsNullOrEmpty(customModelName))
                fullModelName = Path.Combine(folder, string.Format("{0}.xml", customModelName));


            return LoadModelsCollection<T>(fullModelName);
        }


        public T LoadModel<T>(string modelFullName) where T : IStorageEntity
        {
            var ds = new DataContractSerializer(typeof(T));
            var settings = new XmlReaderSettings { };

            using (var r = XmlReader.Create(modelFullName, settings))
            {
                return (T)ds.ReadObject(r);
            }
        }


        public T LoadModel<T>(string folder, string customModelName = null) where T : IStorageEntity
        {
            string fullModelName = Path.Combine(folder, string.Format("{0}.xml", typeof(T).Name));
            if (!string.IsNullOrEmpty(customModelName))
                fullModelName = Path.Combine(folder, string.Format("{0}.xml", customModelName));


            return LoadModel<T>(fullModelName);
        }


        public void SaveModel<T>(string modelFullName, T model)
        {
            var ds = new DataContractSerializer(typeof(T));
            var settings = new XmlWriterSettings { Indent = true };

            using (var w = XmlWriter.Create(modelFullName, settings))
                ds.WriteObject(w, model);
        }


        public void SaveModel<T>(string folder, T model, string customModelName = null)
        {
            var ds = new DataContractSerializer(typeof(T));

            bool isCollection = model is IEnumerable;

            var settings = new XmlWriterSettings { Indent = true };

            string fullModelName = Path.Combine(folder, string.Format("{0}{1}.xml", typeof(T).Name, isCollection ? "Collection" : ""));
            if (!string.IsNullOrEmpty(customModelName))
                fullModelName = Path.Combine(folder, string.Format("{0}.xml", customModelName));

            SaveModel<T>(fullModelName, model);
        }
    }
}

