﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Animals
{
    internal class Enumerator : IEnumerator
    {

        #region Constructor

        public Enumerator(int[] intarr)
        {
            this.m_intArr = intarr;
            m_Cursor = -1;
        }

        #endregion

        #region Properties

        /// <summary>
        ///  Gets the current element in the collection.
        /// </summary>
        public object Current
        {
            get
            {
                if ((m_Cursor < 0) || (m_Cursor == m_intArr.Length))
                    throw new InvalidOperationException();
                return m_intArr[m_Cursor];
            }
        }

        #endregion

        #region Implementation

        /// <summary>
        ///  Sets the enumerator to its initial position, which is before the first element
        ///  in the collection.
        /// </summary>
        public void Reset()
        {
            m_Cursor = -1;
        }

        /// <summary>
        /// Advances the enumerator to the next element of the collection.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            if (m_Cursor < m_intArr.Length)
                m_Cursor++;

            return (!(m_Cursor == m_intArr.Length));
        }

        #endregion

        #region Fields

        private int[] m_intArr;
        private int m_Cursor;

        #endregion
    }
}
