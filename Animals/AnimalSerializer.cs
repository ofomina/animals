﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Animals.Interfaces;
using Animals.Model;
using System.Runtime.Serialization;
using System.Collections;

namespace Animals
{
    internal class AnimalSerializer<T> : IDisposable, ISerializerContract where T : IStorageEntity
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="modelName"></param>
        public AnimalSerializer(DataContractSerializer context, string modelName)
        {
            m_Context = context;
            m_ModelName = modelName;
        }

        #region IDisposable implementation

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool p)
        {
            if (m_Disposed)
                return;
            ///TODO dispose logic here
            ///
            m_Disposed = true;
        }

        ~AnimalSerializer()
        {
            Dispose(false);
        }

        private bool m_Disposed = false;

        #endregion

        #region ISerializerContract implementations

        public void Serialize(object value)
        {
            if (value is AnimalBase)
                using (ISerializerContract serializer = new OneModelAnimalSerializer<T>(m_Context, m_ModelName))
                {
                    serializer.Serialize(value);
                }
            else
            {
                if (((T)value is ICollection))
                    using (ISerializerContract serializer = new ModelCollectionAnimalSerializer<T>(m_Context, m_ModelName))
                    {
                        serializer.Serialize(value);
                    }
                else
                    throw new System.NotSupportedException();
            }
        }

        public object Deserialize()
        {
            if (typeof(T).IsSubclassOf(typeof(AnimalBase)))
                using (ISerializerContract serializer = new OneModelAnimalSerializer<T>(m_Context, m_ModelName))
                {
                    return serializer.Deserialize();
                }
            else
            {
                if (typeof(T).GetInterface("ICollection") != null)
                    using (ISerializerContract serializer = new ModelCollectionAnimalSerializer<T>(m_Context, m_ModelName))
                    {
                        return serializer.Deserialize();
                    }
                else
                    throw new System.NotSupportedException();
            }
        }

        #endregion

        private readonly DataContractSerializer m_Context = default(DataContractSerializer);

        private readonly string m_ModelName = default(string);
    }
}
