﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Animals.Model;
using System.Collections;
using Animals.Interfaces;

namespace Animals
{
    internal class MyCollection : ICollection
    {

        #region Constructor

        public MyCollection()
        {
            m_Count = 3;
        }

        #endregion

        #region Properties

        /// <summary>
        /// The IsSynchronized Boolean property returns True if the 
        /// collection is designed to be thread safe; otherwise, it returns False.
        /// </summary>
        /// 
        public bool IsSynchronized
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// The SyncRoot property returns an object, which is used for synchronizing 
        /// the collection. This returns the instance of the object or returns the 
        /// SyncRoot of other collections if the collection contains other collections.
        /// </summary>
        /// 
        public object SyncRoot
        {
            get
            {
                return this;
            }
        }

        /// <summary>
        /// The Count read-only property returns the number 
        /// of items in the collection.
        /// </summary>
        /// 
        public int Count
        {
            get
            {
                return m_Count;
            }
        }

        #endregion

        #region Implementation

        /// <summary>
        ///  Copies the elements of the System.Collections.ICollection to an System.Array,
        ///  starting at a particular System.Array index.
        /// </summary>
        /// <param name="myArr"></param>
        /// <param name="index"></param>
        /// 
        public void CopyTo(Array myArr, int index)
        {
            foreach (int i in m_intArr)
            {
                myArr.SetValue(i, index);
                index = index + 1;
            }
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerator GetEnumerator()
        {
            return new Enumerator(m_intArr);
        }

        #endregion

        #region Fields

        private int[] m_intArr = { 1, 5, 9 };
        private int m_Count;

        #endregion

    }
}

