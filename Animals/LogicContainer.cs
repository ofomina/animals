﻿/// -----------------------------------------------------------------------
/// <copyright file="LogicContainer.cs"  company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals
{
    #region using

    using System;

    using Animals.Model;
    using Animals.Builder;
    using Animals.Interfaces;
    using Animals.Configurator;


    using Autofac;
    using Autofac.Builder;
   
    #endregion

    /// <summary>
    /// LogicContainer for Animals model
    /// </summary>
    public class LogicContainer: IServiceProviderExt
    {

        /// <summary>
        /// Parameterless constructor
        /// </summary>
        private LogicContainer()
        {
            var builder = new ContainerBuilder();

            ///TODO:
            //if (typeof(T).IsSubclassOf(typeof(AnimalBase)))
            //    builder.RegisterType<AnimalBase>().As<ISerializerContract>();
            //else
            //    if (typeof(T).GetInterface("ICollection") != null && typeof(T).GetInterface("IStorageEntity") != null)
            //        builder.RegisterType<AnimalCollection<AnimalBase>>().As<ISerializerContract>();

            builder.Register(c => (INewAnimal) Activator.CreateInstance(m_AnimalBuilder.Construct("Animals.Model.NewAnimal"))).As<INewAnimal>();
            builder.RegisterType<SpecPanolisFlammea>();

            m_Container = builder.Build(ContainerBuildOptions.None);
        }


        #region Thread safe singletone

        /// <summary>
        /// Returns instance of LogicContainer class
        /// </summary>
        public static IServiceProvider Instance
        {
            get
            {
                lock (__mutex)
                {
                    if (_Instance == null)
                        _Instance = new LogicContainer();
                }
                return _Instance;
            }
        }

        #endregion

        #region Interface implementation

        /// <summary>
        /// Implements IServiceProvider interface
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public object GetService(Type serviceType)
        {
            return m_Container.Resolve(serviceType);
        }

        /// <summary>
        /// Implements IServiceProviderExt interface
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public T GetService<T>(string connectionString)
        {

            m_AnimalCofigurator = new AnimalConfigurator(connectionString);

            var element = m_Container.Resolve<T>();
            m_AnimalCofigurator.Configure<T>(element);

            return element;
        }

        #endregion

        #region Private fields

        private static readonly object __mutex = new object();
        private static LogicContainer _Instance = default(LogicContainer);

        private readonly IContainer m_Container = default(IContainer);
        private readonly AnimalBuilder m_AnimalBuilder = new AnimalBuilder();
        private AnimalConfigurator m_AnimalCofigurator = null;

        #endregion
    }

}
