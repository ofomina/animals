﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Autofac.Builder;
using Animals.Model;
using Animals.Interfaces;
using AutoMapper;
using Application.Module.Infrastructure.Interfaces;
using Application.Module.Infrastructure;

namespace Animals
{
    /// <summary>
    /// LogicContainer
    /// </summary>
    public class LogicContainerOld<T> : IServiceProvider
    {

        static LogicContainer()
        {
            Mapper.CreateMap<object, TypedParameter>().ConvertUsing(new TypedParameterTypeConverter());
        }

        private LogicContainer()
        {
            var builder = new ContainerBuilder();

            if (typeof(T).IsSubclassOf(typeof(AnimalBase)))
                builder.RegisterType<AnimalBase>().As<ISerializerContract>();
            else
                if (typeof(T).GetInterface("ICollection") != null)
                    builder.RegisterType<AnimalCollection<AnimalBase>>().As<ISerializerContract>();

            m_Container = builder.Build(ContainerBuildOptions.Default);
        }

        #region Nested private logic

        private class TypedParameterTypeConverter : ITypeConverter<object, TypedParameter>
        {
            public TypedParameter Convert(ResolutionContext context)
            {
                if (context.SourceValue is IPathResolver)
                    return new TypedParameter(typeof(IPathResolver), context.SourceValue);

                if (context.SourceValue is IDistributedRepository)
                    return new TypedParameter(typeof(IDistributedRepository), context.SourceValue);

                return new TypedParameter(context.SourceType, context.SourceValue);
            }
        }

        #endregion


        #region IServiceProvider implementation

        public object GetService(Type serviceType)
        {

           // if (serviceType == typeof(IDeliveryPackageContract))
           //   return m_Container.Resolve<IDeliveryPackageContract>();

            if (serviceType == typeof(IRepository))
                return m_Container.Resolve<IRepository>();

            return null;
        }

        #endregion


        #region Thread safe singletone

        public static LogicContainer<T> Instance
        {
            get
            {
                lock (__mutex)
                {
                    if (m_Instance == null)
                        m_Instance = new LogicContainer<T>();
                }
                return m_Instance;
            }
        }

        #endregion

        public P GetService<P>()
        {
            return m_Container.Resolve<P>();
        }

        public P GetService<P>(object[] parameters)
        {
            if (parameters == null)
                return GetService<P>();

            var @params = Mapper.Map<object[], IEnumerable<TypedParameter>>(parameters);
            return m_Container.Resolve<P>(@params);
        }


        private static readonly object __mutex = new object();
        private static LogicContainer<T> m_Instance = default(LogicContainer<T>);

        private readonly IContainer m_Container = default(IContainer);
    }
}
