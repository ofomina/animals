﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Animals.Model;
using Animals.Interfaces;
using System.Collections;
using System.Runtime.Serialization;

namespace Animals
{
    internal class AnimalSerializerAutofac<T> : Module where T : IStorageEntity
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="modelName"></param>
        public AnimalSerializerAutofac (DataContractSerializer context, string modelName)
        {
            m_Context = context;
            m_ModelName = modelName;
	    }

        #region Module implementation
        /// <summary>
        /// Methods Load
        /// </summary>
        /// <param name="builder"></param>
        protected override void Load(ContainerBuilder builder)
        {
            if (typeof(T).IsSubclassOf(typeof(AnimalBase)))
                builder.Register(c => new OneModelAnimalSerializer<T>(m_Context, m_ModelName)).As<ISerializerContract>();
            else
            {
                if (typeof(T).GetInterface("ICollection") != null && typeof(T).GetInterface("IStorageEntity") != null)

                    builder.Register(c => new ModelCollectionAnimalSerializer<T>(m_Context, m_ModelName)).As<ISerializerContract>();
            }
        }
        #endregion

        /// <summary>
        /// Fields
        /// </summary>
        private readonly DataContractSerializer m_Context = default(DataContractSerializer);
        private readonly string m_ModelName = default(string);

    }
}