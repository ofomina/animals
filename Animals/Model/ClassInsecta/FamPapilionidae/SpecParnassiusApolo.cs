﻿/// -----------------------------------------------------------------------
/// <copyright file="SpecParnassiusApolo.cs"  company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Model
{

    #region using

    using System;
    using System.Runtime.Serialization;

    #endregion


    /// <summary>
    /// SpecParnassiusApolo (Вид Апполон)
    /// </summary>
    [DataContract]
    public class SpecParnassiusApolo : FamPapilionidae
    {

        /// <summary>
        /// Parameterless Constructor
        /// </summary>
        public SpecParnassiusApolo()
        {
            Species = "ParnassiusApolo";
            MaxWingSpan = 9;
        }


        #region AnimalBase Implementations

        /// <summary>
        /// Implements IAssignable interface
        /// </summary>
        /// <param name="value"></param>
        protected override void vmAssignTo(object value)
        {
            var cls = value as SpecParnassiusApolo;
            if (cls != null)
            {
                base.AssignTo(cls);
            }
        }

        /// <summary>
        /// Implements ICloneable interface
        /// </summary>
        /// <returns></returns>
        protected override object vmClone()
        {
            SpecParnassiusApolo cls = new SpecParnassiusApolo();
            vmAssignTo(cls);
            return cls;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Makes a hissing sound by paws
        /// </summary>
        public void Sizzle()
        {
            throw new NotImplementedException("This method not implemented yet");
        }


        #endregion
    }
}
