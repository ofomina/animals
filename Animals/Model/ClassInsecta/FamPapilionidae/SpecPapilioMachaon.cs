﻿/// -----------------------------------------------------------------------
/// <copyright file="SpecPapilioMachaon.cs"  company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------



namespace Animals.Model
{

    #region using

    using System;
    using System.Runtime.Serialization;


    using Animals.Interfaces;

    #endregion

    /// <summary>
    /// SpecPapilioMachaon (Вид Махаон)
    /// </summary>
    [DataContract]
    public class SpecPapilioMachaon : FamPapilionidae, IEquatable<SpecPapilioMachaon>, IEquatable<string>, IStorageEntity
    {
        /// <summary>
        /// Parameterless Constructor
        /// </summary>
        public SpecPapilioMachaon()
        {
            Species = "PapilioMachaon";
            MaxWingSpan = 8.5;
        }

        /// <summary>
        /// The number of generation during the year
        /// </summary>
        [DataMember(Order = 16, Name = "AnnualGenerationNumber")]
        public byte AnnualGenerationNumber { get { return m_AnnualGenerationNumber; } set { m_AnnualGenerationNumber = value;} }


        #region AnimalBase Implementations

        /// <summary>
        /// Implements IAssignable interface
        /// </summary>
        /// <param name="value"></param>
        protected override void vmAssignTo(object value)
        {
            var cls = value as SpecPapilioMachaon;
            if (cls != null)
            {
                base.AssignTo(cls);
                cls.AnnualGenerationNumber = AnnualGenerationNumber;
            }
        }

        /// <summary>
        /// Implements ICloneable interface
        /// </summary>
        /// <returns></returns>
        protected override object vmClone()
        {
            SpecPapilioMachaon cls = new SpecPapilioMachaon();
            vmAssignTo(cls);
            return cls;
        }

        #endregion

        #region Equals implementation

        /// <summary>
        /// Overrides the Object.Equals(object obj) method
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        /// <summary>
        /// Overrides the Object.GetHashCode() method
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion

        #region IEquatable<PapilioMachaon> implementation

        /// <summary>
        /// Implements IEquatable<PapilioMachaon> interface
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(SpecPapilioMachaon other)
        {
            if (other != null)
                return string.Compare(other.Species, Species) == 0;

            return false;
        }

        #endregion

        #region IEquatable<string> implementation

        /// <summary>
        /// Implements IEquatable<string> interface
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(string other)
        {
            return string.Compare(other, Species) == 0;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Activates defense mechanism from predators
        /// </summary>
        public void DefendFromPredators()
        {
            throw new NotImplementedException("This method not implemented yet");
        }


        #endregion


        private byte m_AnnualGenerationNumber = 2;

    }
}
