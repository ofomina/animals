﻿/// -----------------------------------------------------------------------
/// <copyright file="SpecAnthocharisCardamines.cs"  company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Model
{

    #region using

    using System.Runtime.Serialization;
    using System.Windows.Media;

    #endregion


    /// <summary>
    /// SpecAnthocharisCardamines (Вид Зорька)
    /// </summary>
    [DataContract]
    public class SpecAnthocharisCardamines : FamPieridae
    {

        /// <summary>
        /// Parameterless Constructor
        /// </summary>
        public SpecAnthocharisCardamines()
        {
            Species = "AnthocharisCardamines";
            MaxWingSpan = 4;
        }

        /// <summary>
        /// The color of front wings
        /// </summary>
        [DataMember(Order = 16, Name = "FrontWingsColor")]
        public Colors FrontWingsColor { get { return m_FrontWingsColor; } set { m_FrontWingsColor = value; } }

        /// <summary>
        /// Indicates if front wings have tracery
        /// </summary>
        [DataMember(Order = 17, Name = "FrontWingsHaveTracery")]
        public bool FrontWingsHaveTracery { get { return m_FrontWingsHaveTracery; } set { m_FrontWingsHaveTracery = value; } }
        

        #region AnimalBase Implementations

        /// <summary>
        /// Implements IAssignable interface
        /// </summary>
        /// <param name="value"></param>
        protected override void vmAssignTo(object value)
        {
            var cls = value as SpecAnthocharisCardamines;
            if (cls != null)
            {
                base.AssignTo(cls);
                cls.FrontWingsColor = FrontWingsColor;
                cls.FrontWingsHaveTracery = FrontWingsHaveTracery;
            }
        }

        /// <summary>
        /// Implements ICloneable interface
        /// </summary>
        /// <returns></returns>
        protected override object vmClone()
        {
            SpecAnthocharisCardamines cls = new SpecAnthocharisCardamines();
            vmAssignTo(cls);
            return cls;
        }

        #endregion


        #region Private Fields

        private Colors m_FrontWingsColor = default(Colors);
        private bool m_FrontWingsHaveTracery = true;

        #endregion
    }
}
