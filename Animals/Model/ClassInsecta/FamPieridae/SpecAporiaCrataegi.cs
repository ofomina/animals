﻿/// -----------------------------------------------------------------------
/// <copyright file="SpecAporiaCrataegi.cs"  company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Model
{

    #region using

    using System;
    using System.Runtime.Serialization;

    #endregion


    /// <summary>
    /// SpecAporiaCrataegi (Вид Боярышница)
    /// </summary>
    [DataContract]
    public class SpecAporiaCrataegi : FamPieridae
    {

        /// <summary>
        /// Parameterless Constructor
        /// </summary>
        public SpecAporiaCrataegi()
        {
            Species = "AporiaCrataegi";
            MaxWingSpan = 7;
        }

        /// <summary>
        /// Predominant color of butterfly
        /// </summary>
        [DataMember(Order = 16, Name = "BaseColor")]
        public string BaseColor { get { return m_BaseColor; } set { m_BaseColor = value; } }

        /// <summary>
        /// The length of caterpillar
        /// </summary>
        [DataMember(Order = 17, Name = "CaterpillarLength")]
        public double CaterpillarLength { get { return m_CaterpillarLength; } set { m_CaterpillarLength = value; } }


        #region AnimalBase Implementations

        /// <summary>
        /// Implements IAssignable interface
        /// </summary>
        /// <param name="value"></param>
        protected override void vmAssignTo(object value)
        {
            var cls = value as SpecAporiaCrataegi;
            if (cls != null)
            {
                base.AssignTo(cls);
                cls.BaseColor = BaseColor;
                cls.CaterpillarLength = CaterpillarLength;
            }
        }

        /// <summary>
        /// Implements ICloneable interface
        /// </summary>
        /// <returns></returns>
        protected override object vmClone()
        {
            SpecAporiaCrataegi cls = new SpecAporiaCrataegi();
            vmAssignTo(cls);
            return cls;
        }

        #endregion

        /// <summary>
        /// Migrates from one territory to another habitat
        /// </summary>
        public void Migrate()
        {
            throw new NotImplementedException("This method not implemented yet");
        }


        #region Private Fields

        private string m_BaseColor = default(string);
        private double m_CaterpillarLength = default(double);

        #endregion

    }
}
