﻿/// -----------------------------------------------------------------------
/// <copyright file="SpecDendrolimusSibiricus.cs"  company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------



namespace Animals.Model
{
    #region using

    using System;
    using System.Runtime.Serialization;


    using Animals.Interfaces;

    #endregion


    /// <summary>
    /// SpecDendrolimusSibiricus (Вид Коконопряд сибирский)
    /// </summary>
    [DataContract]
    public class SpecDendrolimusSibiricus : FamLasiocampidae, IWinterSurvive, IHerbivorous
    {
        /// <summary>
        /// Parameterless Constructor
        /// </summary>
        public SpecDendrolimusSibiricus()
        {
            Species = "DendrolimusSibiricus";
        }

        
        /// <summary>
        /// Life cycle duration in months
        /// </summary>
        [DataMember(Order = 16, Name = "LifeSpan")]
        public byte LifeSpan { get { return m_LifeSpan; } set { m_LifeSpan = value; } }


        #region AnimalBase Implementations

        /// <summary>
        /// Implements IAssignable interface
        /// </summary>
        /// <param name="value"></param>
        protected override void vmAssignTo(object value)
        {
            var cls = value as SpecDendrolimusSibiricus;
            if (cls != null)
            {
                base.AssignTo(cls);
                cls.LifeSpan = LifeSpan;
            }
        }

        /// <summary>
        /// Implements ICloneable interface
        /// </summary>
        /// <returns></returns>
        protected override object vmClone()
        {
            SpecDendrolimusSibiricus cls = new SpecDendrolimusSibiricus();
            vmAssignTo(cls);
            return cls;
        }

        #endregion

        
        #region Methods

        /// <summary>
        /// Activates the mechanism of hibernation
        /// </summary>
        public void Hibernate()
        {
            throw new NotImplementedException("This method not implemented yet");
        }


        /// <summary>
        /// Consumes food
        /// </summary>
        public void Feed()
        {
            throw new NotImplementedException("This method not implemented yet");
        }


        #endregion


        private byte m_LifeSpan = 24;

    }
}
