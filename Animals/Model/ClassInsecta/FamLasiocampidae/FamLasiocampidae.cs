﻿/// -----------------------------------------------------------------------
/// <copyright file="FamLasiocampidae.cs"  company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Model
{
    #region using

    using System;
    using System.Runtime.Serialization;


    using Animals.Interfaces;

    #endregion


    /// <summary>
    /// FamLasiocampidae (Семейство Коконопряды)
    /// </summary>
    [DataContract]
    public abstract class FamLasiocampidae : ClassInsecta, ICanFly, ILandInhabit
    {
        /// <summary>
        /// Parameterless Constructor
        /// </summary>
        protected FamLasiocampidae()
        {
            Family = "Lasiocampidae";
            TypeOfEye = eInsectTypeOfEye.Faceted;
            HasWings = true;
            CanFly = true;
        }

        
        /// <summary>
        /// Maximum wingspan in centimeters
        /// </summary>
        [DataMember(Order = 11, Name = "MaxWingSpan")]
        public byte MaxWingSpan { get { return m_MaxWingSpan; } set { m_MaxWingSpan = value; } }


        #region AnimalBase Implementations

        /// <summary>
        /// Implements IAssignable interface
        /// </summary>
        /// <param name="value"></param>
        protected override void vmAssignTo(object value)
        {
            var cls = value as FamLasiocampidae;
            if (cls != null)
            {
                base.AssignTo(cls);
                cls.MaxWingSpan = MaxWingSpan;
            }
        }

        #endregion


        #region Methods

        /// <summary>
        /// Weaves cocoon from spiderweb
        /// </summary>
        public void WeaveSilkCocoon()
        {
            throw new NotImplementedException("This method not implemented yet");
        }


        /// <summary>
        /// Transforms from a caterpillar into a butterfly
        /// </summary>
        public void Transform()
        {
            throw new NotImplementedException("This method not implemented yet");
        }


        #endregion

        private byte m_MaxWingSpan = default(byte);
    }
}
