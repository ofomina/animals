﻿/// -----------------------------------------------------------------------
/// <copyright file="SpecMalacosomaNeustria.cs"  company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------



namespace Animals.Model
{

    #region using

    using System;
    using System.Runtime.Serialization;

    #endregion


    /// <summary>
    /// SpecMalacosomaNeustria (Вид Коконопряд кольчатый)
    /// </summary>
    [DataContract]
    public class SpecMalacosomaNeustria : FamLasiocampidae
    {

        /// <summary>
        /// Parameterless Constructor
        /// </summary>
        public SpecMalacosomaNeustria()
        {
            Species = "MalacosomaNeustria";
            MaxWingSpan = 4;
        }


        #region AnimalBase Implementations

        /// <summary>
        /// Implements IAssignable interface
        /// </summary>
        /// <param name="value"></param>
        protected override void vmAssignTo(object value)
        {
            var cls = value as SpecMalacosomaNeustria;
            if (cls != null)
            {
                base.AssignTo(cls);
                cls.MaxWingSpan = MaxWingSpan;
            }
        }

        /// <summary>
        /// Implements ICloneable interface
        /// </summary>
        /// <returns></returns>
        protected override object vmClone()
        {
            SpecMalacosomaNeustria cls = new SpecMalacosomaNeustria();
            vmAssignTo(cls);
            return cls;
        }

        #endregion

        /// <summary>
        /// Lays eggs spiral shaped
        /// </summary>
        public void DoSpiralLayingEggs()
        {
            throw new NotImplementedException("This method not implemented yet");
        }

        
    }
}
