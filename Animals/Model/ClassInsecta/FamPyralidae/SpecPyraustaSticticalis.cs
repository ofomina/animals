﻿/// -----------------------------------------------------------------------
/// <copyright file="SpecPyraustaSticticalis.cs"  company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------



namespace Animals.Model
{

    #region using

    using System;
    using System.Runtime.Serialization;

    #endregion


    /// <summary>
    /// SpecPyraustaSticticalis (Вид Мотылек луговой)
    /// </summary>
    [DataContract]
    public class SpecPyraustaSticticalis : FamPyralidae
    {

        /// <summary>
        /// Parameterless Constructor
        /// </summary>
        public SpecPyraustaSticticalis()
        {
            Species = "PyraustaSticticalis";
            MaxWingSpan = 3.5;
        }

        /// <summary>
        /// The number of generations during one year
        /// </summary>
        [DataMember(Order = 16, Name = "AnnualGenerationsNumber")]
        public int AnnualGenerationsNumber { get { return m_AnnualGenerationsNumber; } set { m_AnnualGenerationsNumber = value; } }


        #region AnimalBase Implementations

        /// <summary>
        /// Implements IAssignable interface
        /// </summary>
        /// <param name="value"></param>
        protected override void vmAssignTo(object value)
        {
            var cls = value as SpecPyraustaSticticalis;
            if (cls != null)
            {
                base.AssignTo(cls);
                cls.AnnualGenerationsNumber = AnnualGenerationsNumber;
            }
        }

        /// <summary>
        /// Implements ICloneable interface
        /// </summary>
        /// <returns></returns>
        protected override object vmClone()
        {
            SpecPyraustaSticticalis cls = new SpecPyraustaSticticalis();
            vmAssignTo(cls);
            return cls;
        }

        #endregion

        /// <summary>
        /// Transforms from caterpillar into butterfly
        /// </summary>
        public void Transform()
        {
            throw new NotImplementedException("This method not implemented yet");
        }


        #region Private Fields

        private int m_AnnualGenerationsNumber;

        #endregion

    }
}
