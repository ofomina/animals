﻿/// -----------------------------------------------------------------------
/// <copyright file="SpecGalleriaMellonella.cs"  company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Model
{

    #region using

    using System;
    using System.Runtime.Serialization;
    using System.Windows.Media;

    #endregion


    /// <summary>
    /// SpecGalleriaMellonella (Вид Огнёвка восковая)
    /// </summary>
    [DataContract]
    public class SpecGalleriaMellonella : FamPyralidae
    {

        /// <summary>
        /// Parameterless Constructor
        /// </summary>
        public SpecGalleriaMellonella()
        {
            Species = "GalleriaMellonella";
            MaxWingSpan = 3.5;
        }

        /// <summary>
        /// The color of front wings
        /// </summary>
        [DataMember(Order = 16, Name = "FrontWingsColor")]
        public Colors FrontWingsColor { get { return m_FrontWingsColor; } set { m_FrontWingsColor = value; } }

        /// <summary>
        /// The color of rear wings
        /// </summary>
        [DataMember(Order = 17, Name = "RearWingsColor")]
        public Colors RearWingsColor { get { return m_RearWingsColor; } set { m_RearWingsColor = value; } }

        
        #region AnimalBase Implementations

        /// <summary>
        /// Implements IAssignable interface
        /// </summary>
        /// <param name="value"></param>
        protected override void vmAssignTo(object value)
        {
            var cls = value as SpecGalleriaMellonella;
            if (cls != null)
            {
                base.AssignTo(cls);
                cls.FrontWingsColor = FrontWingsColor;
                cls.RearWingsColor = RearWingsColor;
            }
        }

        /// <summary>
        /// Implements ICloneable interface
        /// </summary>
        /// <returns></returns>
        protected override object vmClone()
        {
            SpecGalleriaMellonella cls = new SpecGalleriaMellonella();
            vmAssignTo(cls);
            return cls;
        }

        #endregion

        /// <summary>
        /// Uses wax for food
        /// </summary>
        public void EatWax()
        {
            throw new NotImplementedException("This method not implemented yet");
        }


        #region Private Fields

        private Colors m_FrontWingsColor = default(Colors);
        private Colors m_RearWingsColor = default(Colors);

        #endregion


    }
}
