﻿/// -----------------------------------------------------------------------
/// <copyright file="ClassInsecta.cs"  company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


/// <summary>
/// Enumerates all possible types of insect's eye
/// </summary>
public enum eInsectTypeOfEye {
    /// <summary>
    /// Faceted types of eye
    /// </summary>
    Faceted,
    /// <summary>
    /// Simple types of eye
    /// </summary>
    Simple };

namespace Animals.Model
{

    #region using

    using System;
    using System.Runtime.Serialization;


    using Animals.Interfaces;

    #endregion


    /// <summary>
    /// Insecta (Класс Насекомые)
    /// </summary>
    [DataContract]
    public abstract class ClassInsecta : AnimalBase, ICanSee
    {
        /// <summary>
        /// Parameterless Constructor
        /// </summary>
        protected ClassInsecta() 
        {
            Class = "Insecta";
        }

        #region Properties

        /// <summary>
        /// Determines if an insect has wings
        /// </summary>
        [DataMember(Order = 6, Name = "HasWings")]
        public bool HasWings { get { return m_HasWings; } set { m_HasWings = value; } }

        /// <summary>
        /// Determines if an insect can fly
        /// </summary>
        [DataMember(Order = 7, Name = "CanFly")]
        public bool CanFly { get { return m_CanFly; } set { m_CanFly = value; } }

        /// <summary>
        /// Insect's type of eye
        /// </summary>
        [DataMember(Order = 8, Name = "TypeOfEye")]
        public eInsectTypeOfEye TypeOfEye { get { return m_TypeOfEye; } set { m_TypeOfEye = value; } }

        #endregion


        #region AnimalBase Implementations

        /// <summary>
        /// Implements IAssignable interface
        /// </summary>
        /// <param name="value"></param>
        protected override void vmAssignTo(object value)
        {
            var cls = value as ClassInsecta;
            if (cls != null)
            {
                base.AssignTo(cls);
                cls.HasWings = HasWings;
                cls.CanFly = CanFly;
                cls.TypeOfEye = TypeOfEye;
            }
        }

        #endregion
 
        /// <summary>
        /// Visually perceives the objects of outward things
        /// </summary>
        public void See()
        {
            throw new NotImplementedException("This method not implemented yet");
        }


        #region Private Fields

        private bool m_HasWings = default(bool);
        private bool m_CanFly = default(bool);
        private eInsectTypeOfEye m_TypeOfEye = default(eInsectTypeOfEye);

        #endregion

    }
}
