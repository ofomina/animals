﻿/// -----------------------------------------------------------------------
/// <copyright file="FamNoctuidae.cs"  company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Model
{

    #region using

    using System;
    using System.Runtime.Serialization;


    using Animals.Interfaces;

    #endregion


    /// <summary>
    /// FamNoctuidae (Семейство Совки)
    /// </summary>
    [DataContract]
    public abstract class FamNoctuidae : ClassInsecta, IHerbivorous, ICanFly, ILandInhabit
    {
        /// <summary>
        /// Parameterless Constructor
        /// </summary>
        protected FamNoctuidae()
        {
            Family = "Noctuidae";
            TypeOfEye = eInsectTypeOfEye.Faceted;
            HasWings = true;
            CanFly = true;
        }

        /// <summary>
        /// Maximum wingspan in centimeters
        /// </summary>
        [DataMember(Order = 11, Name = "MaxWingSpan")]
        public double MaxWingSpan { get { return m_MaxWingSpan; } set { m_MaxWingSpan = value; } }


        #region AnimalBase Implementations

        /// <summary>
        /// Implements IAssignable interface
        /// </summary>
        /// <param name="value"></param>
        protected override void vmAssignTo(object value)
        {
            var cls = value as FamNoctuidae;
            if (cls != null)
            {
                base.AssignTo(cls);
                cls.MaxWingSpan = MaxWingSpan;
            }
        }

        #endregion


        #region Methods

        /// <summary>
        /// Consumes food
        /// </summary>
        public void Feed()
        {
            throw new NotImplementedException("This method not implemented yet");
        }


        /// <summary>
        /// Transforms from caterpillar into butterfly
        /// </summary>
        public void Transform()
        {
            throw new NotImplementedException("This method not implemented yet");
        }


        #endregion


        private double m_MaxWingSpan = default(byte);
    }
}
