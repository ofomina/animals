﻿/// -----------------------------------------------------------------------
/// <copyright file="SpecPanolisFlammea.cs"  company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------



namespace Animals.Model
{
    #region using

    using System;
    using System.Runtime.Serialization;

    #endregion


    /// <summary>
    /// SpecPanolisFlammea (Вид Совка сосновая)
    /// </summary>
    [DataContract]
    public class SpecPanolisFlammea : FamNoctuidae
    {
        /// <summary>
        /// Parameterless Constructor
        /// </summary>
        public SpecPanolisFlammea()
        {
            Species = "PanolisFlammea";
            MaxWingSpan = 3.5;
        }


        /// <summary>
        /// The number of molts during life cycle
        /// </summary>
        [DataMember(Order = 16, Name = "MoltsNumber")]
        public byte MoltsNumber { get { return m_MoltsNumber; } set { m_MoltsNumber = value; } }


        #region AnimalBase Implementations

        /// <summary>
        /// Implements IAssignable interface
        /// </summary>
        /// <param name="value"></param>
        protected override void vmAssignTo(object value)
        {
            var cls = value as SpecPanolisFlammea;
            if (cls != null)
            {
                base.AssignTo(cls);
                cls.MoltsNumber = MoltsNumber;
            }
        }

        /// <summary>
        /// Implements ICloneable interface
        /// </summary>
        /// <returns></returns>
        protected override object vmClone()
        {
            SpecPanolisFlammea cls = new SpecPanolisFlammea();
            vmAssignTo(cls);
            return cls;
        }

        #endregion

        
        /// <summary>
        /// Changes its cover
        /// </summary>
        public void Molt()
        {
            throw new NotImplementedException("This method not implemented yet");
        }

        

        private byte m_MoltsNumber = 4;
    }
}
