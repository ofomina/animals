﻿/// -----------------------------------------------------------------------
/// <copyright file="SpecTapinostolaMusculosa.cs"  company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------



namespace Animals.Model
{

    using System.Runtime.Serialization;

    /// <summary>
    /// SpecTapinostolaMusculosa (Вид Совка стеблевая)
    /// </summary>
    [DataContract]
    public class SpecTapinostolaMusculosa : FamNoctuidae
    {
        /// <summary>
        /// Parameterless Constructor
        /// </summary>
        public SpecTapinostolaMusculosa()
        {
            Species = "TapinostolaMusculosa";
        }


        #region AnimalBase Implementations

        /// <summary>
        /// Implements IAssignable interface
        /// </summary>
        /// <param name="value"></param>
        protected override void vmAssignTo(object value)
        {
            var cls = value as SpecTapinostolaMusculosa;
            if (cls != null)
            {
                base.AssignTo(cls);
            }
        }

        /// <summary>
        /// Implements ICloneable interface
        /// </summary>
        /// <returns></returns>
        protected override object vmClone()
        {
            SpecTapinostolaMusculosa cls = new SpecTapinostolaMusculosa();
            vmAssignTo(cls);
            return cls;
        }

        #endregion
 
    }
}
