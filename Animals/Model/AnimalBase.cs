﻿/// -----------------------------------------------------------------------
/// <copyright file="AnimalBase.cs"  company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------



namespace Animals.Model
{

    #region using

    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;


    using Animals.Interfaces;

    #endregion

    /// <summary>
    /// Abstract model for animals
    /// </summary>
    [DataContract, KnownType("GetKnownTypes")]
    public abstract class AnimalBase : IAssignable, ICloneable, IDisposable, IStorageEntity
    {
        /// <summary>
        /// Parameterless Constructor
        /// </summary>
        protected AnimalBase() {}


        #region Properties

        /// <summary>
        /// The name of animal's class
        /// </summary>
        [DataMember(Order = 1, Name = "Class")]
        public string Class { get { return m_Class; } protected set { m_Class = value; } }

        /// <summary>
        /// The name of animal's family
        /// </summary>
        [DataMember(Order = 2, Name = "Family")]
        public string Family { get { return m_Family; } protected set { m_Family = value; } }

        /// <summary>
        /// The name of animal's species
        /// </summary>
        [DataMember(Order = 3, Name = "Species")]
        public string Species { get { return m_Species; } protected set { m_Species = value; } }

        #endregion


        #region IAssignable implementation

        /// <summary>
        /// Implements IAssignable interface using abstract method that will be overloaded in inheritor
        /// </summary>
        /// <param name="value"></param>
        public void AssignTo(object value)
        {
            vmAssignTo(value);
        }

        /// <summary>
        /// Abstract method that implements IAssignable interface and will be overloaded in inheritor
        /// </summary>
        /// <param name="value"></param>
        protected abstract void vmAssignTo(object value);

        #endregion

        #region ICloneable implementation

        /// <summary>
        /// Implements ICloneable interface using abstract method that will be overloaded in inheritor
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return vmClone();
        }

        /// <summary>
        /// Abstract method that implements ICloneable interface and will be overloaded in inheritor
        /// </summary>
        /// <returns></returns>
        protected abstract object vmClone();

        #endregion

        #region IDisposable implementation

        /// <summary>
        /// Implements IDisposable interface
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Implements IDisposable interface
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.m_Disposed)
            {
                if (disposing)
                {
                    ///Dispose some objects
                }
                m_Disposed = true;
            }
        }

        private bool m_Disposed = false;

        #endregion

        #region Type Factory

        /// <summary>
        /// Implements type factory
        /// </summary>
        /// <typeparam name="EntType"></typeparam>
        public static void Register<EntType>()
        where EntType : AnimalBase
        {
            lock (__mutex)
            {
                var type = typeof(EntType);
                if (_KnownTypes.IndexOf(type) < 0)
                    _KnownTypes.Add(type);
            }
        }

        private static IEnumerable<Type> GetKnownTypes() { return _KnownTypes; }

        #endregion

        /// <summary>
        /// Reproduces a new instance of the species
        /// </summary>
        public virtual void Reproduce() 
        {
            throw new NotImplementedException("This method not implemented yet");
        }


        #region Fields

        public static readonly List<Type> _KnownTypes = new List<Type>();

        private string m_Class;
        private string m_Family;
        private string m_Species;

        private static readonly object __mutex = new object();

        #endregion

     }
}
