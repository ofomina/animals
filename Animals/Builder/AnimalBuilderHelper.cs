﻿/// -----------------------------------------------------------------------
/// <copyright file="AnimalBuilderConfig.cs" company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Builder
{
    #region using

    using System;
    using System.Reflection;
    using System.Reflection.Emit;


    using Animals.Configuration;


    #endregion

    /// <summary>
    /// AnimalModel configuration section type configurator
    /// </summary>
    internal class AnimalBuilderHelper
    {
        /// <summary>
        /// Configures dynamic type using AnimalModel configuration section
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        /// <exception cref="NotSupportedException("type is not supported")"
        /// <exception cref="InvalidOperationException("type definition error");"
        public Type Construct(string typeName)
        {
            Type typeToConfig = null;
 

            SpeciesCollection speciesItems = ConfigBuffer.SpeciesItems;
            SpeciesElement speciesElement = null;

            foreach (var species in speciesItems)
            {
                if (((SpeciesElement)species).Name.Equals(typeName))
                {
                    speciesElement = (SpeciesElement)species;
                    break;
                }
            }

            if (speciesElement == null)
                throw new NotSupportedException(string.Format("type {0} is not supported", typeName));

            try
            {

                AssemblyName assemblyName = new AssemblyName("AnimalModelConfig, Version=1.0.0.1");
                AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(
                            assemblyName,
                            AssemblyBuilderAccess.RunAndSave);

                ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule(assemblyName.Name, assemblyName.Name + ".dll");

                // Collect a list of interfaces implemented by the type
                Type[] speciesInterfaces = {};

                foreach (var speciesInterface in ((SpeciesElement)speciesElement).SpeciesInterfaces)
                {
                    Array.Resize(ref speciesInterfaces, speciesInterfaces.Length + 1);
                    Type type = Type.GetType(((SpeciesInterfaceElement)speciesInterface).Name);
                    speciesInterfaces[speciesInterfaces.Length - 1] = type;
                }

                TypeBuilder typeBuilder = moduleBuilder.DefineType(typeName, TypeAttributes.Public, typeof(AnimalBuilder), speciesInterfaces);

                Type propertyType = null;
                String propertyName = "";
                    String methodName = "";

                /// Define properties
                foreach (var property in ((SpeciesElement)speciesElement).SpeciesProperties)
                {
                    propertyType = Type.GetType(((SpeciesPropertyElement)property).Type);
                    propertyName = ((SpeciesPropertyElement)property).Name;

                    // Define a private field fro property
                    FieldBuilder fieldBuilder = typeBuilder.DefineField(
                            "m_" + propertyName,
                            propertyType,
                            FieldAttributes.Private);

                    // Define a property that gets and sets the private field.
                    PropertyBuilder propertyBuilder = typeBuilder.DefineProperty(
                            propertyName,
                            PropertyAttributes.HasDefault,
                            propertyType,
                            null);

                    // The property "set" and property "get" methods require a special
                    // set of attributes.
                    MethodAttributes getSetAttr = MethodAttributes.Public |
                        MethodAttributes.SpecialName | MethodAttributes.HideBySig;

                    // Define the "get" accessor method
                    MethodBuilder propertyGetAccessor = typeBuilder.DefineMethod(
                        "get_" + propertyName,
                        getSetAttr,
                        propertyType,
                        Type.EmptyTypes);

                    ILGenerator propertyGetIL = propertyGetAccessor.GetILGenerator();
                    // For an instance property, argument zero is the instance. Load the 
                    // instance, then load the private field and return, leaving the
                    // field value on the stack.
                    propertyGetIL.Emit(OpCodes.Ldarg_0);
                    propertyGetIL.Emit(OpCodes.Ldfld, fieldBuilder);
                    propertyGetIL.Emit(OpCodes.Ret);

                    // Define the "set" accessor method, which has no return
                    // type and takes one argument of type.
                    MethodBuilder propertySetAccessor = typeBuilder.DefineMethod(
                        "set_" + propertyName,
                        getSetAttr,
                        null,
                        new Type[] { propertyType });

                    ILGenerator propertySetIL = propertySetAccessor.GetILGenerator();
                    // Load the instance and then the property argument, then store the
                    // argument in the field.
                    propertySetIL.Emit(OpCodes.Ldarg_0);
                    propertySetIL.Emit(OpCodes.Ldarg_1);
                    propertySetIL.Emit(OpCodes.Stfld, fieldBuilder);
                    propertySetIL.Emit(OpCodes.Ret);

                    // Last, map the "get" and "set" accessor methods to the 
                    // PropertyBuilder. The property is now complete. 
                    propertyBuilder.SetGetMethod(propertyGetAccessor);
                    propertyBuilder.SetSetMethod(propertySetAccessor);
                }

                // Define a default constructor that supplies a default value
                // for the private field.
                ConstructorBuilder ctor0 = typeBuilder.DefineConstructor(
                    MethodAttributes.Public,
                    CallingConventions.Standard,
                    Type.EmptyTypes);

                ILGenerator ctor0IL = ctor0.GetILGenerator();

                // For a constructor, argument zero is a reference to the new instance.
                ctor0IL.Emit(OpCodes.Ldarg_0);
                ctor0IL.Emit(OpCodes.Ldc_I4_S, 42);
                ctor0IL.Emit(OpCodes.Call, typeof(object).GetConstructor(Type.EmptyTypes));
                ctor0IL.Emit(OpCodes.Ret);

                // Define methods
                foreach (var method in ((SpeciesElement)speciesElement).SpeciesMethods)
                {
                    methodName = ((SpeciesMethodElement)method).Name;

                    // Define a method
                    MethodBuilder methodBuilder = typeBuilder.DefineMethod(
                        methodName,
                        MethodAttributes.Public,
                        null,
                        null);

                    ILGenerator methIL = methodBuilder.GetILGenerator();
                    methIL.Emit(OpCodes.Ldarg_0);
                    methIL.Emit(OpCodes.Ret);

                }

                // Finish the type.
                typeToConfig = typeBuilder.CreateType();
            }
            catch (TypeLoadException err)
            {
                throw new TypeLoadException(string.Format("type {0} load error", typeName), err);
            }

            return typeToConfig;
        }

    }
}
