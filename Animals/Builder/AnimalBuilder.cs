﻿/// -----------------------------------------------------------------------
/// <copyright file="AnimalBuilder.cs" company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Builder
{
    #region using

    using System;
    using System.Collections.Generic;
    using Animals.Interfaces;

    #endregion

    /// <summary>
    /// Returns existen type or constructs dynamic type
    /// using AnimalModel configuration section
    /// </summary>
    public class AnimalBuilder
    {

        #region ITypeBuilder implementation

        /// <summary>
        /// Implements ITypeBuilder interface
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public Type Construct(string typeName)
        {
            Type typeToConstruct = Type.GetType(typeName);

            if (typeToConstruct == null)
            {
                AnimalBuilderHelper animalBuilder = new AnimalBuilderHelper();
                typeToConstruct = animalBuilder.Construct(typeName);
            }

            return typeToConstruct;
                   
            throw new NotImplementedException();
        }

        #endregion

        /// TRASH:
        //internal void Configure<T1>(object tt)
        //{
        //    Type confType = typeof(T1);

        //    if(confs.ContainsKey(confType))
        //    {ITypeBuilder builder =  confs[confType] as ITypeBuilder;

        //        //builder.Construct()
        //    }
        //}

        //Dictionary<Type, object> confs;
    }
}
