﻿/// -----------------------------------------------------------------------
/// <copyright file="AnimalModelConfigSection.cs" company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Configuration
{
    #region using

    using System.Configuration;


    #endregion

    /// <summary>
    /// Class associated with "AnimalModel" section within a configuration file
    /// </summary>
    public class AnimalModelConfigSection : ConfigurationSection
    {
        /// <summary>
        /// Collection of species
        /// </summary>
        [ConfigurationProperty("SpeciesList")]
        public SpeciesCollection SpeciesItems
        {
            get { return ((SpeciesCollection)(base["SpeciesList"])); }
        }
    }
}
