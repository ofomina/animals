﻿/// -----------------------------------------------------------------------
/// <copyright file="SpeciesCollection.cs" company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Configuration
{
    #region using

    using System.Configuration;


    #endregion

    /// <summary>
    /// Class associated with "SpeciesList" section within a configuration file
    /// </summary>
    [ConfigurationCollection(typeof(SpeciesElement), AddItemName = "Species")]
    public class SpeciesCollection : ConfigurationElementCollection
    {

        #region ConfigurationElementCollection virtual methods implementation

        /// <summary>
        /// Creates new SpeciesElement
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new SpeciesElement();
        }

        /// <summary>
        /// Returns key property value for SpeciesElement
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SpeciesElement)(element)).SpeciesProperties;
        }

        /// <summary>
        /// Implements indexer for SpeciesCollection
        /// </summary>
        /// <param name="idx"></param>
        /// <returns></returns>
        public SpeciesElement this[int idx]
        {
            get { return (SpeciesElement)BaseGet(idx); }
        }

        #endregion
    }
}
