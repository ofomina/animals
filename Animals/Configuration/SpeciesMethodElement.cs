﻿/// -----------------------------------------------------------------------
/// <copyright file="SpeciesMethodElement.cs" company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Configuration
{
    #region using

    using System.Configuration;

    #endregion

    /// <summary>
    /// Class associated with "Method" section within a configuration file
    /// </summary>
    public class SpeciesMethodElement : ConfigurationElement
    {

        /// <summary>
        /// Name of the method
        /// </summary>
        [ConfigurationProperty("methodName", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return ((string)(base["methodName"])); }
            set { base["methodName"] = value; }
        }
    }
}
