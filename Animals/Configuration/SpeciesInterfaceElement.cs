﻿/// -----------------------------------------------------------------------
/// <copyright file="SpeciesInterfaceElement.cs" company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Configuration
{
    #region using

    using System.Configuration;

    #endregion

    /// <summary>
    /// Class associated with "Interface" section within a configuration file
    /// </summary>
    public class SpeciesInterfaceElement : ConfigurationElement
    {

        /// <summary>
        /// Name of the Interface
        /// </summary>
        [ConfigurationProperty("interfaceName", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return ((string)(base["interfaceName"])); }
            set { base["interfaceName"] = value; }
        }
    }
}
