﻿/// -----------------------------------------------------------------------
/// <copyright file="SpeciesElement.cs" company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Configuration
{
    #region using

    using System.Configuration;
    
    using Animals.Interfaces;
    using Animals.Model;


    #endregion

    //public class FamLasiocampidaeSpe : SpeciesElement, IEntityConfigurator<FamLasiocampidae>
    //{
    //    public void Configure(FamLasiocampidae element)
    //    {
    //        base.Configure(element);
    //        //
    //        throw new System.NotImplementedException();
    //    }
    //}

    /// <summary>
    /// Class associated with "Species" section within a configuration file
    /// </summary>
    public class SpeciesElement : ConfigurationElement //, IEntityConfigurator<AnimalBase>
    {

        /// <summary>
        /// The name of species
        /// </summary>
        [ConfigurationProperty("speciesName", DefaultValue = null, IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return ((string)(base["speciesName"])); }
            set { base["speciesName"] = value; }
        }
        
        /// <summary>
        /// Species interfaces collection
        /// </summary>
        [ConfigurationProperty("Interfaces", DefaultValue = null, IsKey = false, IsRequired = false)]
        public SpeciesInterfacesCollection SpeciesInterfaces
        {
            get { return ((SpeciesInterfacesCollection)(base["Interfaces"])); }
            set { base["Interfaces"] = value; }
        }

        /// <summary>
        /// Species properties collection
        /// </summary>
        [ConfigurationProperty("Properties", DefaultValue = null, IsKey = false, IsRequired = false)]
        public SpeciesPropertiesCollection SpeciesProperties 
        {
            get { return ((SpeciesPropertiesCollection)(base["Properties"])); }
            set { base["Properties"] = value; }
        }

        /// <summary>
        /// Species methods collection
        /// </summary>
        [ConfigurationProperty("Methods", DefaultValue = null, IsKey = false, IsRequired = false)]
        public SpeciesMethodsCollection SpeciesMethods
        {
            get { return ((SpeciesMethodsCollection)(base["Methods"])); }
            set { base["Methods"] = value; }
        }

        //public void Configure(AnimalBase element)
        //{
        //    //element.Class = 
        //}

    }
}
