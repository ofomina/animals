﻿/// -----------------------------------------------------------------------
/// <copyright file="ConfigBuffer.cs" company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Configuration
{
    #region using

    using System;
    using System.Configuration;
    using SoftServe.Svyaznoy.DbCommon;

    #endregion

    /// <summary>
    /// Configuration AnimalModel section buffer
    /// </summary>
    public static class ConfigBuffer
    {

        /// <summary>
        /// Connection string to specified client configuration file
        /// Format: "FilePath=<FilePath>; ConfigurationUserLevel=<ConfigurationUserLevel>"
        /// </summary>
        public static string ConnectionString { get { return m_ConnectionString; } set { m_ConnectionString = value; } }
        
        /// <summary>
        /// Species types
        /// </summary>
        public static SpeciesCollection SpeciesItems { 
            get
            {
                if (!ConfigBuffer.IsInitialized)
                    ConfigBuffer.Initialize();
                
                return m_SpeciesItems;
            }
        }

        /// <summary>
        /// Determines if buffer is initialized
        /// </summary>
        public static bool IsInitialized { get { return m_IsInitialized; } }

        /// <summary>
        /// Loads configuration AnimalModel section
        /// </summary>
        public static void Initialize()
        {
            try
            {
                m_IsInitialized = false;
                System.Configuration.Configuration config = null;

                if (!String.IsNullOrEmpty(ConnectionString))
                {

                    string filePath = default(string);
                    ConfigurationUserLevel configurationUserLevel = default(ConfigurationUserLevel);

                    // Parse connection string to get output file path and configuration user level
                    ArgumentParser parser = new ArgumentParser(ConnectionString, ';');

                    try
                    {
                        while (parser.Read())
                        {
                            if (string.Compare(parser.Name, "FilePath", true) == 0)
                            {
                                filePath = (string)parser.Value;
                                continue;
                            }
                            if (string.Compare(parser.Name, "ConfigurationUserLevel", true) == 0)
                            {
                                configurationUserLevel = (ConfigurationUserLevel)(Enum.Parse(typeof(ConfigurationUserLevel), (string)parser.Value, true)); ;
                                continue;
                            }
                        }

                    }
                    catch (InvalidOperationException err)
                    {
                        throw new InvalidOperationException("connection string parse error", err);
                    }


                    // Map the new configuration file
                    ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
                    configFileMap.ExeConfigFilename = filePath;

                    // Get the mapped configuration file
                    config = ConfigurationManager.OpenMappedExeConfiguration(configFileMap, configurationUserLevel);
                }
                else
                    config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                
                AnimalModelConfigSection animalModelConfigSection = (AnimalModelConfigSection)config.GetSection("AnimalModel");
                m_SpeciesItems = animalModelConfigSection.SpeciesItems;
                m_IsInitialized = true;
            }
            catch (InvalidOperationException err)
            {
                throw new InvalidOperationException("Loading configuration AnimalModel section error", err);
            }
        }


        #region Private fields

        private static string m_ConnectionString = default(string);
        private static SpeciesCollection m_SpeciesItems = default(SpeciesCollection);
        private static bool m_IsInitialized = false;

        #endregion
    }
}
