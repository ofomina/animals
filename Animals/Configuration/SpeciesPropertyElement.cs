﻿/// -----------------------------------------------------------------------
/// <copyright file="SpeciesPropertyElement.cs" company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Configuration
{
    #region using

    using System.Configuration;


    #endregion

    /// <summary>
    /// Class associated with "Property" section within a configuration file
    /// </summary>
    public class SpeciesPropertyElement : ConfigurationElement
    {

        /// <summary>
        /// Name of the property
        /// </summary>
        [ConfigurationProperty("propertyName", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return ((string)(base["propertyName"])); }
            set { base["propertyName"] = value; }
        }

        /// <summary>
        /// Type of the property
        /// </summary>
        [ConfigurationProperty("propertyType", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Type
        {
            get { return ((string)(base["propertyType"])); }
            set { base["propertyType"] = value; }
        }

        /// <summary>
        /// Property value
        /// </summary>
        [ConfigurationProperty("propertyValue", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string Value
        {
            get { return ((string)(base["propertyValue"])); }
            set { base["propertyValue"] = value; }
        }

    }
}
