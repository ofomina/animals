﻿/// -----------------------------------------------------------------------
/// <copyright file="SpeciesMethodsCollection.cs" company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Configuration
{
    #region using

    using System.Configuration;


    #endregion

    /// <summary>
    /// Class associated with "Methods" section within a configuration file
    /// </summary>
    [ConfigurationCollection(typeof(SpeciesMethodElement), AddItemName = "Method")]
    public class SpeciesMethodsCollection : ConfigurationElementCollection
    {

        #region ConfigurationElementCollection virtual methods implementation

        /// <summary>
        /// Creates new SpeciesMethodElement
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new SpeciesMethodElement();
        }

        /// <summary>
        /// Returns key property value for SpeciesMethodElement
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SpeciesMethodElement)(element)).Name;
        }

        #endregion

        /// <summary>
        /// Implements indexer for SpeciesMethodsCollection
        /// </summary>
        /// <param name="idx"></param>
        /// <returns></returns>
        public SpeciesMethodElement this[int idx]
        {
            get { return (SpeciesMethodElement)BaseGet(idx); }
        }

    }
}
