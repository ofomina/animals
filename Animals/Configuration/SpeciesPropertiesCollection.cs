﻿/// -----------------------------------------------------------------------
/// <copyright file="SpeciesPropertiesCollection.cs" company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Configuration
{
    #region using

    using System.Configuration;


    #endregion

    /// <summary>
    /// Class associated with "Properties" section within a configuration file
    /// </summary>
    [ConfigurationCollection(typeof(SpeciesPropertyElement), AddItemName = "Property")]
    public class SpeciesPropertiesCollection: ConfigurationElementCollection
    {
        
        #region ConfigurationElementCollection virtual methods implementation

        /// <summary>
        /// Creates new SpeciesPropertyElement
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new SpeciesPropertyElement();
        }

        /// <summary>
        /// Returns key property value for SpeciesPropertyElement
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey( ConfigurationElement element )
        {
            return ((SpeciesPropertyElement)(element)).Name;
        }

        #endregion

        /// <summary>
        /// Implements indexer for SpeciesPropertiesCollection
        /// </summary>
        /// <param name="idx"></param>
        /// <returns></returns>
        public SpeciesPropertyElement this[int idx]
        {
            get { return (SpeciesPropertyElement)BaseGet(idx); }
        }

    }
}
