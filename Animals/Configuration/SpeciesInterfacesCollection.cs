﻿/// -----------------------------------------------------------------------
/// <copyright file="SpeciesInterfacesCollection.cs" company="SoftServe">
/// Copyright SoftServe, Inc 2013. All Rights Reserved. 
/// This file contains material protected under International and Federal 
/// Copyright Laws and Treaties. Any unauthorized reprint or use of this 
/// material is prohibited. No part of this file may be reproduced or 
/// transmitted in any form or by any means, electronic or mechanical,
/// including photocopying, recording, or by any information storage and 
/// retrieval system without express written permission from the author / owner
/// </copyright>
/// -----------------------------------------------------------------------


namespace Animals.Configuration
{
    #region using

    using System.Configuration;


    #endregion

    /// <summary>
    /// Class associated with "Interfaces" section within a configuration file
    /// </summary>
    [ConfigurationCollection(typeof(SpeciesInterfaceElement), AddItemName = "Interface")]
    public class SpeciesInterfacesCollection : ConfigurationElementCollection
    {

        #region ConfigurationElementCollection virtual methods implementation

        /// <summary>
        /// Creates new SpeciesInterfaceElement
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new SpeciesInterfaceElement();
        }

        /// <summary>
        /// Returns key property value for SpeciesInterfaceElement
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SpeciesInterfaceElement)(element)).Name;
        }

        #endregion

        /// <summary>
        /// Implements indexer for SpeciesInterfacesCollection
        /// </summary>
        /// <param name="idx"></param>
        /// <returns></returns>
        public SpeciesInterfaceElement this[int idx]
        {
            get { return (SpeciesInterfaceElement)BaseGet(idx); }
        }

    }
}
