﻿
namespace Animals.Interfaces
{
    using System;

    public interface IServiceProviderExt : IServiceProvider
    {
        T GetService<T>(string connectionString);
    }
}
