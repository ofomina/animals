﻿//------------------------------------------------------------------------------
// <copyright file="ArgumentPacer.cs" company="SoftServe">
//     Copyright (c) SoftServe in-Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace SoftServe.Svyaznoy.DbCommon
{
    using System;
    using System.Text;

#if TEST
    public 
#else
    internal
#endif
    class ArgumentParser
    {
        private enum eReadMode { Name, Value, String, CDDATA };

        public enum eDataType { I1, I2, I4, I8, Double, Money, Bool, UIC, String, DateTime, Struct, Empty, Error };

        private const string Numbers = "0123456789.,+-e";
        private const char CONST_SPACE = ' ';
        private const char CONST_CONTROL = '\\';
        private const char CONST_NEXT_LINE = '\n';
        private const char CONST_GO_START = '\r';
        private const char CONST_TAB = '\t';
        private const char CONST_TERMINATOR = '\0';
        private const string DELIMS = "~!@$%^&*()-+={}[]/* ";

        public ArgumentParser(string data, char argDelim)
        {
            m_argDelim = argDelim;
            m_data = data;
            m_position = 0;
        }

        public string CDDATAStart { get { return @"<![CDATA["; } }
        public string CDDATAEnd { get { return @"]]>"; } }
        public char ArgDelimiter { get { return m_argDelim; } }
        public bool StrongMode { get; set; }
        public bool TypeDetection { get; set; }

        public string Name { get; private set; }
        public object Value { get; private set; }

        public void Reset() { m_position = 0; }

        public bool Read()
        {
            Value = null;
            Name = string.Empty;
            
            int subCDDATA = 0; 
            eReadMode readMode = eReadMode.Name;
            
            StringBuilder paramName = new StringBuilder(5);
            StringBuilder paramValue = new StringBuilder();
            StringBuilder stringValue = new StringBuilder(10);

            while (m_position < m_data.Length)
            {
                char ch = m_data[m_position];
                if (readMode == eReadMode.Name)
                {
                    if (DELIMS.IndexOf(ch) > -1)
                    {
                        if (paramName.Length < 1)
                        {
                            if (ch == CONST_SPACE)
                            {
                                __skipSpeaces(ref m_data, ref m_position);
                                continue;
                            }
                            throw new InvalidOperationException("parcer error: unexpected symbol");
                        }
                        else
                        {
                            if (ch == CONST_SPACE)
                                __skipSpeaces(ref m_data, ref m_position);
                            m_position++;

                            Name = paramName.ToString();
                            paramName.Clear();
                            paramValue.Clear();
                            readMode = eReadMode.Value;
                        }
                        continue;
                    }
                    paramName.Append(ch);
                    m_position++;
                    continue;
                }
                if (readMode == eReadMode.Value)
                {
                    if (ch == CONST_SPACE)
                    {
                        if (!__skipSpeaces(ref m_data, ref m_position))
                            continue; // stream was end
                        ch = m_data[m_position];
                    }
                    if (ch == CONST_CONTROL)
                    {
                        m_position++;
                        if (m_position < m_data.Length)
                        {
                            paramValue.Append(m_data[m_position]);
                            m_position++;
                            continue;
                        }
                        throw new InvalidOperationException("parcer error: unexpected end of stream");
                    }

                    if (ch == '"')
                    {
                        //if (!string.IsNullOrEmpty(paramValue))
                        //    throw new InvalidOperationException("parcer error: unexpected symbol");

                        readMode = eReadMode.String; // switch to string mode and continue                      
                        m_position++;
                        continue;
                    }
                    if (ch == '<') // allowed any string symbol
                    {
                        if (__tryReadCDDATAHeader(ref m_data, ref m_position))
                        {
                            //if(!string.IsNullOrEmpty( paramValue))
                            //    throw new InvalidOperationException("parcer error: unexpected symbol");

                            m_position += CDDATAStart.Length;
                            readMode = eReadMode.CDDATA;
                            stringValue.Clear();
                            continue;
                        }
                    }

                    if (ch == m_argDelim)
                    {
                        eDataType tCode = eDataType.Empty;
                        if (paramValue.Length < 1) // No value
                            Value = null;
                        else
                            Value = __typeDetection(paramValue, out tCode);

                        paramValue.Clear();
                        m_position++;

                        return true;
                    }
                    paramValue.Append(ch);
                    m_position++;
                    continue;
                }

                if (readMode == eReadMode.String)
                {
                    if (ch == CONST_CONTROL)
                    {
                        m_position++;
                        if (m_position < m_data.Length)
                        {
                            paramValue.Append(m_data[m_position]);
                            m_position++;
                            continue;
                        }
                        throw new InvalidOperationException("parcer error: unexpected end of stream");
                    }
                    if (ch == '"')
                    {
                        readMode = eReadMode.Value; // switch back to Value mode;
                        paramValue.Append(stringValue);
                        stringValue.Clear();
                        m_position++;
                        continue;
                    }
                    stringValue.Append(ch);
                    m_position++;
                    continue;
                }
                if (readMode == eReadMode.CDDATA)
                {
                    if (ch == '<' && __tryReadCDDATAHeader(ref m_data, ref m_position))
                    {
                        //if(!string.IsNullOrEmpty( paramValue))
                        //    throw new InvalidOperationException("parcer error: unexpected symbol");

                        subCDDATA += 1;
                        stringValue.Append(CDDATAStart);
                        m_position += CDDATAStart.Length;
                        continue;
                    }
                    if (ch == ']' && __tryReadCDDATAEnd(ref m_data, ref m_position))
                    {
                        //if(!string.IsNullOrEmpty( paramValue))
                        //    throw new InvalidOperationException("parcer error: unexpected symbol");

                        subCDDATA -= 1;
                        if (subCDDATA < 0)
                        {
                            readMode = eReadMode.Value; // switch back to Value mode;
                            paramValue.Append(stringValue);
                            stringValue.Clear();
                        }
                        else
                        {
                            stringValue.Append(CDDATAEnd);
                            m_position--;
                        }
                        m_position += CDDATAEnd.Length + 1;
                        continue;
                    }
                    stringValue.Append(ch);
                    m_position++;
                }
            }

            if (readMode == eReadMode.Value)
            {
                eDataType tCode = eDataType.Empty;
                if (paramValue.Length < 1)
                    Value = null;
                else Value = __typeDetection(paramValue, out tCode);

                return true;
            }
            if (readMode == eReadMode.String | readMode == eReadMode.CDDATA)
            {
                if (StrongMode)
                    throw new InvalidOperationException("parcer error: unexpected end of stream");

                eDataType tCode = eDataType.Empty;
                paramValue.Append(stringValue);
                if (paramValue.Length > 0)
                    Value = __typeDetection(paramValue, out tCode);

                return true;
            }
            return false;
        }

        internal bool __skipSpeaces(ref string data, ref int position)
        {
            if (data == null)
                return false;
            while (position < data.Length)
            {
                position++;
                if (data[position] != CONST_SPACE)
                    break;
            }
            return position < data.Length;
        }
        internal bool __tryReadCDDATAHeader(ref string data, ref int position)
        {
            if (data == null)
                return false;

            if (position + CDDATAStart.Length - 1 < data.Length)
                return string.Compare(CDDATAStart, data.Substring(position, CDDATAStart.Length), true) == 0;

            return false;
        }
        internal bool __tryReadCDDATAEnd(ref string data, ref int position)
        {
            if (data == null)
                return false;

            if (position + CDDATAEnd.Length - 1 < data.Length)
                return string.Compare(CDDATAEnd, data.Substring(position, CDDATAEnd.Length), true) == 0;

            return false;
        }
        internal object __typeDetection(StringBuilder value, out eDataType tCode)
        {
            tCode = eDataType.Empty;
            if (!TypeDetection)
                return value.ToString();          
             
            object dwValue = null;
            if (__tryReadValue(value, out tCode, out dwValue))
                return dwValue;
            
            throw new NotSupportedException();
        }

        internal bool __tryReadValue(StringBuilder value, out eDataType type, out object dwValue)
        {
            dwValue = null;
            type = eDataType.Empty;
            //const string NumberSuffixs = "LlMmFf";
            
            try
            {
                int lenght = value.Length;

                #region detect empty/nullable
                
                if (lenght < 1)
                    return true;

                #endregion
                
                
                #region detect number



                #endregion
                return true;
            }
            catch (Exception err)
            {
                dwValue = err;
                type = eDataType.Error;
                return false;
            }
        }

        private string m_data = default(string);
        private char m_argDelim = default(char);
        private int m_position = default(int);

    }
}
